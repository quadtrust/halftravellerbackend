module Api
  module V1
    class HomeController < Api::BaseController
      def index
        render json: {
          test: 'This should not be visible to the unauthenticated user!!'
        }
      end
    end
  end
end
