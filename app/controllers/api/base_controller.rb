# This is the Base controller that is extending the Application controller.
module Api
  class BaseController < ApplicationController
    # Lets have all the API's Locked.
    acts_as_token_authentication_handler_for User, except: %i[token_session password]
  end
end
