class DashboardController < ApplicationController
  def token_register
    user = User.new(user_params)
    if user.save
      render json: user.as_json(authentication_token: user.authentication_token, email: user.email), status: 201
    else
      warden.custom_failure!
      render json: user.errors, status: 422
    end
  end

  # This is the function for the token sending to the user.
  def token_session
    email = params[:user][:email]
    username = params[:user][:username]
    password = params[:user][:password]
    user = User.authenticate(email || username, password)
    # sleep 2
    if user.nil?
      data = {
        error: 'wrong credentials'
      }
      render json: data, status: 401
    else
      data = {
        authentication_token: user.authentication_token,
        username: user.email
      }
      render(json: data, status: 201) && return
    end
  end

  private

  def user_params
    params.require(:user).permit(:username, :email, :password)
  end
end
