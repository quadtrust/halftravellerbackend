Rails.application.routes.draw do
 devise_for :users
 root 'home#index'
 post 'token' => 'dashboard#token_session'
 post 'register' => 'dashboard#token_register'

 namespace :api do
  namespace :v1 do
    get 'home' => 'home#index'
  end
 end
end
